<?php

namespace Drush\Commands\force_translation_update;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drush\Commands\DrushCommands;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Force locale update on updatedb.
 */
class ForceTranslationUpdateCommand extends DrushCommands implements SiteAliasManagerAwareInterface
{
    use SiteAliasManagerAwareTrait;

  /**
   * Add force-translation-update option.
   *
   * @hook option *
   *
   * @param \Symfony\Component\Console\Command\Command     $command
   * @param \Consolidation\AnnotatedCommand\AnnotationData $annotationData
   */
    public function forceTranslationUpdateOption(Command $command, AnnotationData $annotationData): void
    {
        $command->addOption(
            'force-translation-update',
            '',
            InputOption::VALUE_NONE,
            'Force update of translations after updatedb.'
        );
    }

  /**
   * Forced update of translation files.
   * @hook post-command updatedb
   * @param $result
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   */
    public function forceTranslationUpdateCommand($result, CommandData $commandData): void
    {
        if ($result !== 0) {
            return;
        }

        if ($commandData->input()->getOption('force-translation-update') !== true) {
            return;
        }

        $self = $this->siteAliasManager()->getSelf();
        $manager = $this->processManager();

      // Check if locale module is enabled.
        $pmListOptions = ['type' => 'module', 'status' => 'enabled', 'format' => 'php'];
        $process = $manager->drush($self, 'pm:list', [], $pmListOptions);
        $process->mustRun();
        $output = unserialize($process->getOutput(), ['allowed_classes' => false]);

        if (!isset($output['locale'])) {
            return;
        }

        $this->io()->block('Updating translations after updatedb.', 'INFO', 'fg=green');

      // Without these two calls, the locale:check command will not update
      // the translation files.
        locale_translation_clear_status();
        locale_translation_file_history_delete();
        $process = $manager->drush($self, 'locale:check');
        $process->mustRun($process->showRealtime());
        $process = $manager->drush($self, 'locale:update');
        $process->mustRun($process->showRealtime());
    }
}
